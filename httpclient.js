var File = Java.type('java.io.File')
var Scanner = Java.type('java.util.Scanner')
var URL = Java.type('java.net.URL')
var URLConnection = Java.type('java.net.URLConnection')
var StandardCharsets = Java.type('java.nio.charset.StandardCharsets')
var TrustManager = Java.type('javax.net.ssl.TrustManager')
var X509TrustManager = Java.type('javax.net.ssl.X509TrustManager')
var SSLContext = Java.type('javax.net.ssl.SSLContext')
var HttpsURLConnection = Java.type('javax.net.ssl.HttpsURLConnection')
var HostnameVerifier = Java.type('javax.net.ssl.HostnameVerifier')


function mount_http_request(method, url, params) {
    var request = this
    var params = params || {}
    var properties = {
        charset: StandardCharsets.UTF_8,
        "Content-Type": "application/x-www-form-urlencoded"
    }

    var fluent = {
        params: (function (pars) {
            if (isObject(params) && isObject(pars)) {
                params = merge(params, pars)
            } else {
                params = pars
            }
            return fluent
        }).bind(request),

        property: (function (property, value) {
            properties[property] = value

            return fluent
        }).bind(request),

        charset: (function (value) {
            properties["Accept-Charset"] = value

            return fluent
        }).bind(request),

        contentType: (function (value) {
            properties["Content-Type"] = value

            return fluent
        }).bind(request),

        get_content: function (http_connection) {
            try {
                return {
                    body: inputStreamToString(http_connection.getInputStream()),
                    responseCode: http_connection.getResponseCode()
                }
            } catch (e) {
                var body
                try {
                    body = inputStreamToString(http_connection.getErrorStream())
                } catch (error) {
                    body = http_connection.getResponseMessage()
                }
                return {
                    body: body,
                    responseCode: http_connection.getResponseCode()
                } 
            }
        },
        fetch: (function _fetch() {
            var http_connection

            if (method.toUpperCase() === "POST") {
                var output;

                http_connection = new URL(url).openConnection()
                http_connection.setRequestMethod(method)

                http_connection.setDoOutput(true)
                
                if (isObject(params)) {
                    if (properties["Content-Type"] === 'application/json') {
                        if (Object.keys(params).length) {
                            params = JSON.stringify(params) 
                        } else {
                            params = ''
                        }
                    } else if (properties["Content-Type"] === 'application/x-www-form-urlencoded') {
                        params = http.serializeParams(params || {})
                    }
                }
                
                Object.keys(properties).forEach(function (propXYZ) {
                    http_connection.setRequestProperty(propXYZ, properties[propXYZ])
                })

                if (params) {
                    output = http_connection.getOutputStream()
                    output.write(params.getBytes(properties.charset))
                }

            } else /* if (method.toUpperCase() == "GET") */ { // E OS OUTROS METODOS HTTP ??
                url += "?" + http.serializeParams(params || {})
                http_connection = new URL(url).openConnection()
                Object.keys(properties).forEach(function (propXYZ) {
                    http_connection.setRequestProperty(propXYZ, properties[propXYZ])
                })
            }


            return fluent.get_content(http_connection)
        }).bind(request)
    }

    return fluent
}


/**
 * Busca informações em uma api REST
 * @example
 * // returns {nome: "David", idade: 10}
 * HTTPClient.post("http://localhost:8080/test/pecho").charset("UTF-8").fetch()
 * @example
 * // returns "Hello World!"
 * HTTPClient.post("http://localhost:8080/test/hello").fetch()
 */
var HTTPClient = {
    get: function (url, params) { return mount_http_request("GET", url, params) },
    post: function (url, params) { return mount_http_request("POST", url, params) },
    put: function (url, params) { return mount_http_request("PUT", url, params) },
    delete: function (url, params) { return mount_http_request("DELETE", url, params) },
}

exports = HTTPClient

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns obj3 a new object based on obj1 and obj2
 */
function merge(obj1, obj2) {
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

function isObject(val) {
    return val && typeof val === 'object'
}

function inputStreamToString(inputStream) {
    var scanner = new Scanner(inputStream, "UTF-8")
    var content = scanner.useDelimiter("\\Z|\\A").next()
    scanner.close()

    return content
}
